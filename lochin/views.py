from rest_framework import viewsets

from .models import Product, Category, Orderitem, Order, OrderType
from .serializer.order import OrderCreateSerializer, OrderTypeCreateSerializer
from .serializer.product import ProductListSerializer, CategoryListSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderCreateSerializer



class OrderTypeViewSet(viewsets.ModelViewSet):
    queryset = OrderType.objects.all()
    serializer_class = OrderTypeCreateSerializer

# class ProductListView(generics.ListCreateAPIView):
#
#     queryset = Product.objects.all()
#     serializer_class = ProductListSerializer
#
# class ProductDetailView(generics.RetrieveUpdateDestroyAPIView):
#
#     queryset = Product.objects.all()
#     serializer_class = ProductDetailSerializer


# class CategoryListView(APIView):
#
#     def get(self, request):
#         category = Category.objects.all()
#         serializer = CategoryListSerializer(category, many=True)
#         return Response(serializer.data)
#
#
# class CategoryDetailView(APIView):
#
#     def get(self, request, pk):
#         category = Category.objects.get(id=pk)
#         serializer = CategoryDetailSerializer(category)
#         return Response(serializer.data)
#
#
# class OrderIntemCreateView(APIView):
#
#     def post(self, request):
#         orderItem = OrderItemCreateSerializer(data=request.data)
#         if orderItem.is_valid():
#             orderItem.save()
#         return Response(status=201)

# from django.shortcuts import render
# from rest_framework.response import Response
# from rest_framework.views import APIView
# # Create your views here.
#
# from .models import Product, Category
# from .serializer import ProductListSerializer, CategoryListSerializer
#
#
# class ProductListView(APIView):
#
#     def get(self, request):
#         products = Product.objects.all()
#         serializer = ProductListSerializer(products, many=True)
#         return Response(serializer.data)
#
#
# class ProductDetailView(APIView):
#
#     def get(self, requestm, pk):
#         products = Product.objects.get(id = pk)
#         serializer = ProductListSerializer(products)
#         return Response(serializer.data)
#
#
# class CategoryListView(APIView):
#
#     def get(self, request):
#         category = Category.objects.all()
#         serializer = CategoryListSerializer(category, many=True)
#         return Response(serializer.data)
