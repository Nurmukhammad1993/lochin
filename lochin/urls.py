from django.urls import path, include

# from . import views

from rest_framework.routers import DefaultRouter
from .views import ProductViewSet, CategoryViewSet, OrderViewSet, OrderTypeViewSet

router = DefaultRouter()
router.register('product', ProductViewSet, basename='product')
router.register('category', CategoryViewSet, basename='category')
router.register('order', OrderViewSet, basename='order')
router.register('orderType', OrderTypeViewSet, basename='orderType')


urlpatterns = [

    path('', include(router.urls))
    # path("product/", views.ProductListView.as_view()),
    # path("product/<int:pk>/", views.ProductDetailView.as_view()),
    # path("category/", views.CategoryListView.as_view()),
    # path("category/<int:pk>", views.CategoryDetailView.as_view()),
    # path("orderItem/", views.OrderIntemCreateView.as_view()),
]










# from . import views
#
# urlpatterns = [
#     path("product/", views.ProductListView.as_view()),
#     path("category/", views.CategoryListView.as_view()),
#     path("product/<int:pk>/", views.ProductDetailView.as_view())
# ]
#
