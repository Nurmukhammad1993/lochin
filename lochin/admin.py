from django.contrib import admin
from .models import Category, User, Client, Order, Orderitem,Product, OrderType
# Register your models here.

admin.site.register(Category)
admin.site.register(User)
admin.site.register(Client)
admin.site.register(Order)
admin.site.register(Orderitem)
admin.site.register(Product)
admin.site.register(OrderType)
