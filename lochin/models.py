from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class Category(models.Model):
    name = models.CharField("Категория", max_length=150)
    description = models.TextField("Описание")
    url = models.SlugField(max_length=160, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Product(models.Model):
    name = models.CharField('Название продукта', max_length=50)
    description = models.TextField("Описание")
    url = models.SlugField(max_length=160, unique=True)
    country = models.CharField("Страна", max_length=30)
    brand = models.CharField("Бренд", max_length=100)
    package = models.CharField("Упаковка", max_length=100)
    type = models.CharField("Тип", max_length=100)
    price = models.PositiveIntegerField("Цена")
    vendor_code = models.PositiveIntegerField("Артикул")
    image = models.ImageField("Фото", upload_to="productsPhoto/", null=True)

    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, related_name="categories")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"


class Client(models.Model):
    name = models.CharField("Имя", max_length=150)
    surname = models.CharField("Фамилия", max_length=150)
    phone = models.PositiveIntegerField("Номер")
    address = models.CharField("Адрес", max_length=200)
    photo = models.ImageField("Фото", upload_to="usersPhoto/", null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"


class User(AbstractUser):
    client = models.OneToOneField(Client, verbose_name="Клиент", related_name='client_name', on_delete=models.CASCADE,
                                  null=True)

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class OrderType(models.Model):
    type = models.CharField("Тип Оплаты", max_length=50)

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = "Тип"
        verbose_name_plural = "Типы"


class Order(models.Model):
    totalPriceAmount = models.PositiveIntegerField("Общая цена")

    orderType = models.ForeignKey(OrderType, on_delete=models.CASCADE, related_name='orderTypes')

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='users')

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"


class Orderitem(models.Model):
    productQuantaty = models.PositiveIntegerField("Количество продуктов")
    price = models.PositiveIntegerField("Цена")

    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='orders')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='products')

    def __str__(self):
        return str(self.id)
