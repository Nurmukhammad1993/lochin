from rest_framework import serializers

from lochin.models import Product, Category


class ProductListSerializer(serializers.ModelSerializer):
    category = serializers.SlugField('name', read_only=True)

    class Meta:
        model = Product
        fields = "__all__"


class CategoryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = "__all__"
