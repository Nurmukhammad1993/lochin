from collections import defaultdict

from django.db import transaction
from rest_framework import serializers

from lochin.models import Orderitem, Order, OrderType, Product, User
from django.db.models import Q


class OrderTypeCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderType
        fields = "__all__"


class OrderItemSerializer(serializers.Serializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    quantity = serializers.IntegerField()

    # lists = []
    # def validate_product(self, value):
    #
    #     if value in self.lists:
    #         print('test', self.lists)
    #         raise serializers.ValidationError('A genre with this name already exists.')
    #
    #     self.lists.append(value)
    #     return value


class OrderItemListSerializer(serializers.Serializer):
    # id = serializers.PrimaryKeyRelatedField(queryset=Orderitem.objects.all())
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    quantity = serializers.IntegerField(source='productQuantaty')


class OrderCreateSerializer(serializers.ModelSerializer):
    order_items = OrderItemSerializer(many=True, required=False)

    class Meta:
        model = Order
        fields = "__all__"

    def validate_order_items(self, values):
        user = self.context['request'].user
        print(user)
        print('Enter validate_order_items', values)
        errors = list()
        product_list = list()
        for value in values:
            product = value.get('product', None)
            print(product)
            product_list.append(product)
        if len(set(product_list)) != len(product_list):
            errors.append('product are duplicated')

        if errors:
            raise serializers.ValidationError(errors)
        return values

    def validate(self, attrs):
        print('Enter validate')
        errors = defaultdict(list)
        print(attrs)

        # order_items=attrs.get('order_items',[])
        #
        # print(order_items,'order_items')
        # print(self.context['request'].user,'user in validate')
        # # print(self.context['request'].user.client, 'client')

        if errors:
            raise serializers.ValidationError(errors)
        return attrs

    def create(self, validated_data):
        user = self.context['request'].user

        validated_data['user'] = user

        order_items = validated_data.pop('order_items')  ## wto bi ne powel v model Order

        order = Order.objects.create(**validated_data)

        # for item in order_items:
        #
        #         Orderitem.objects.create(
        #             order=order,
        #             product=item['product'],
        #             productQuantaty=item['quantity'],
        #             price=item['product'].price
        #
        #         )

        order_items_list = list()

        for item in order_items:
            product_id = item.pop('product', None)
            productQuantaty = item.pop('quantity', None)
            price = product_id.price

            order_items_list.append(Orderitem(
                order=order,
                product=product_id,
                productQuantaty=productQuantaty,
                price=price))

        Orderitem.objects.bulk_create(order_items_list)

        return order

    @transaction.atomic
    def update(self, instance, validated_data):

        order_items = validated_data.pop('order_items', [])
        print("validated data test", validated_data)
        print("instance data test", instance)

        instance.totalPriceAmount = validated_data.get('totalPriceAmount', instance.totalPriceAmount)
        instance.orderType = validated_data.get('orderType', instance.orderType)
        instance.user = validated_data.get('user', instance.user)
        instance.save()


        current_orderItem_id_list = list()


        for item in order_items:
            Orderitem.objects.update_or_create(
                order=instance,
                product=item['product'],
                defaults=dict(productQuantaty=item['quantity'], price=item['product'].price)
            )

            current_orderItem_id_list.append(Orderitem.objects.get(
                order=instance,
                product=item['product']
            ).id)



        Orderitem.objects.filter(~Q(id__in = current_orderItem_id_list) & Q(order = instance)).delete()


        return instance


    def to_representation(self, instance):
        self.fields['orders'] = OrderItemListSerializer(many=True, required=False)
        return super(OrderCreateSerializer, self).to_representation(instance)
